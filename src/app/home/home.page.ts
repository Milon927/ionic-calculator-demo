import { Component } from '@angular/core';
import { present } from '@ionic/core/dist/types/utils/overlays';

// tslint:disable: no-debugger
// tslint:disable: prefer-for-of
// tslint:disable: prefer-const
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor() {}
  math = '0';
  oldValue = '0';
  readyForNewInput = true;
  dot = true;
  lastOperator = null;
  result = '';
  signChange = true;
  toDelete = null;
  lastAns = null;
  onNumberPress(num)
  {
    //debugger;
    if (this.math.length >= 21)
      {
        return;
      }
    if ( typeof num === 'number') {
      if (this.readyForNewInput) {
        this.math = '' + num;
      } else {
        this.math += '' + num;
      }
      this.readyForNewInput = false;
    } else {
      if (this.dot) {
        this.math += '' + num;
        this.dot = false;
      }
      return;
    }
    // const formalMath = this.formalizeDigit(this.math);
    const formalMath = this.math.split(',').join('');
    const result = this.calculation(formalMath, 'n');
    this.result = Number(result) ? this.formalizeDigit (result.toString()) : ' ';
    this.toDelete = null;
  }
  formalizeDigit(math) {
    let arr;
    let digit;
    if (math.includes('.')) {
      math = math.split(',').join('');
    }
    if (math.includes('.'))
    {
      arr = (math + '').split('.');
      digit = parseFloat(arr[0].toString());
      math = digit.toLocaleString() + '.' + arr[1];
    }
    else {
      if (math.length === 16)
      {
        console.log(math[0]);
        console.log(math.substring(1, 4));
        math = math[0] + ',' + math.substring(1, 4) +
        ',' + math.substring(4, 7) + ',' + math.substring(7, 10)
        + ',' + math.substring(10, 13) + ',' + math.substring(13, 16);
      }
      else {
        digit = parseFloat(math.toString());
        math = digit.toLocaleString();
      }
    }
    return math;
  }
  onButtonPress(symbol)
  {   if (symbol === 'percent') {
    debugger;
    let percent = '';
    if(this.math === '0') {
      return;
    }
    if (!this.math.includes('+') && !this.math.includes('-') && !this.math.includes('÷') && !this.math.includes('×')) {
      this.result = (parseFloat(this.math) / 100).toString();
      this.math = this.result;
      this.result = this.result.toLocaleString();
      return;
    }
    for(let p=this.math.length-1; p>=0;p--) {       
      if(this.math.charAt(p) === '+' || this.math.charAt(p) === '-' || this.math.charAt(p) === '÷' || this.math.charAt(p) === '×' )
      {
        this.math = this.math.substring(0,p+1);
        break;
      } else {
        percent += '' + this.math.charAt(p);
      }
    }
    percent = percent.split("").reverse().join("");
    if(percent.length !==0) {
      this.math += (parseFloat(percent) / 100).toString();
      this.result = this.calculation(this.math, 's');
      this.result = this.result.toLocaleString();
    }
      // this.result = ((parseFloat(this.math) / 100) * parseFloat(this.oldValue)).toString();
      // this.oldValue = this.result;
      // this.lastOperator = symbol;
      return;
    } else if (symbol === 'ce') {
      this.math = '0';
      this.readyForNewInput = true;
      return;
    } else if (symbol === 'c') {
      this.math = '0';
      this.oldValue = '0';
      this.result = '';
      this.readyForNewInput = true;
      return;
    } else if (symbol === 'backspace') {
      if (this.toDelete === null) {
        this.math = this.math.slice(0, -1);
      }
      else  {
        this.math = this.math.slice(0, this.toDelete - 1 ) + this.math.slice(this.toDelete);
      }
      this.calculation(this.math, 's');
      return;
    } else if (symbol === 'inverse') {
      let value = this.calculation(this.math, 's');
      this.result = (1 / parseFloat(value)).toString();
      return;
    } else if (symbol === 'square') {
      this.math = this.math.split(',').join('');
      let square = this.calculation(this.math, 's');
      square = Math.pow(parseFloat(square), 2);
      this.result = square.toLocaleString();
      return;
    } else if (symbol === 'squareroot') {
      this.math = this.math.split(',').join('');
      const square = Math.sqrt(parseFloat(this.math));
      this.math = square.toLocaleString();
      return;
    } else if (symbol === 'divide') {
      this.math += '\u00F7';
    } else if (symbol === 'multiple') {
      this.math += '\u00D7';
    } else if (symbol === 'minus') {
      this.math += '\u002D';
    } else if (symbol === 'plus') {
      this.math += '\u002B';
    } else if (symbol === 'sign') {
      if (this.signChange) {
        this.math = '-' + this.math;
        this.signChange = !this.signChange;
      } else {
        this.math = this.math.substring(1);
      }
    } else if (symbol === 'equal') {
      this.readyForNewInput = true;
    } else if (symbol === 'Ans') {
      if (this.lastAns === null) {
        // coding next for toast
      } else {
        this.math += ''+'Ans';
        this.result = this.calculation(this.math, 's');
      }
    } else {

    }
      this.oldValue = this.math;
  }

  calculation(formalMath, task) {
  // debugger;
    const x = formalMath.split(',').join('');    
    var rawArr = [];
    var mulArr = [];
    var addArr = [];
    var subArr = [];
    var resultArr = [];
    var num1 = null;    
    let result = 0;
    if ( ((Number(x[x.length - 1]) || x[x.length - 1] === '0')  && x[x.length - 1] !== '.') || x[x.length - 1] === 's' ) {
      for ( let i = 0; i < x.length; i++) {
        if ( Number(x[i]) || x[i] === '.' || x[i] === '0') {
          if ( num1 === null) {
          num1 = x[i];
        } else {
          num1 += x[i];
        }
      } else if (x[i] === 'A'){
        let ans = this.lastAns;
        if(Number(x[i-1]|| x[i - 1] === '0')) {
          rawArr.push('×'); 
        }
        rawArr.push(ans);        
        if(Number(x[i+3])) {
          rawArr.push('×'); 
        }
        i += 2; 
       } else {        
        rawArr.push(num1);        
        rawArr.push(x[i]);
        num1 = null;
        }
      }
     // debugger;
        rawArr.push(num1);
        console.log(rawArr);

        if(rawArr.includes('÷') && rawArr.length > 2) {
          mulArr = this.division(rawArr);
          if(mulArr.includes('×') && mulArr.length > 2) {
              addArr = this.multiplication(mulArr);
              if( (addArr.includes('+') || addArr.includes('-') )&& addArr.length > 2) {
                  let res = this.resultCal(addArr);
                  resultArr.push(res);
              } else {
                  resultArr.push(addArr[0]);
              }
          } else {
              if((mulArr.includes('+') || addArr.includes('-') )&& mulArr.length > 2) {
                  let res = this.resultCal(mulArr);
                  resultArr.push(res);
              } else {
                  resultArr.push(mulArr[0]);
              }
          }
      } else {
          if(rawArr.includes('×') && rawArr.length > 2) {
              addArr = this.multiplication(rawArr);
              if((addArr.includes('+') || addArr.includes('-') ) && addArr.length > 2) {
                  let res = this.resultCal(addArr);
                  resultArr.push(res);
              } else {
                  resultArr.push(addArr[0]);
              }
          } else {
              if((rawArr.includes('+') || rawArr.includes('-') )&& rawArr.length > 2) {
                  let res = this.resultCal(rawArr);
                  resultArr.push(res);
              } else {
                  resultArr.push(rawArr[0]);
              }
          }
      }
      if ( rawArr.length >= 1 && task === 's')
      {
        return resultArr[0];
      }
      if(rawArr.length > 1 && task === 'n')
      {
        return resultArr[0];
      }
      return '';
}


 rawArr = []; 
  }
  todo(e: any) {
   // debugger;
    this.toDelete = e.target.selectionStart;
    console.log(this.toDelete);
  }


   divCal(val1, val2) {
    return +val1 / +val2;
}
 mulCal(val1, val2) {
    return +val1 * +val2;
}
 addCal(val1, val2) {
    return +val1 + +val2;
}
 subCal(val1, val2) {
    return +val1 - +val2;
}

 division(rawArr) {

// for all division
let c = 0;
let mulArr = [];
while(c< rawArr.length) 
 { 
	if(rawArr[c] === '÷') 
	{
		var v1 = rawArr[c-1];
		var v2 = rawArr[c+1];
		var result = this.divCal(v1,v2);
		mulArr.pop();
		mulArr.push(result);
		c = c+2;
	} 
	else {
	mulArr.push(rawArr[c]);
	c++;
	}
 }
return mulArr; 

}


 multiplication(mulArr) {
let m = 0;
let addArr = [];
// for all multiplication
 
 while(m< mulArr.length) 
 { 
	if(mulArr[m] === '×') 
	{
		var v1 = mulArr[m-1];
		var v2 = mulArr[m+1];
		var result = this.mulCal(v1,v2);
		addArr.pop();
		addArr.push(result);
		m = m+2;
	} 
	else {
	addArr.push(mulArr[m]);
	m++;
	}
 } 
return addArr;
}




 resultCal(addArr) {
   debugger;
let result = 0;
let numArr = [];
let symbolArr = [];
for(var i = 0; i< addArr.length; i++) {
if(Number(addArr[i]) || addArr[i] === '0')
{	
    numArr.push(addArr[i]);
}
else {	
	symbolArr.push(addArr[i]);
}
}
result = numArr[0];
for(var c = 0 ; c< numArr.length - 1; c++) {
   // debugger;
    if(symbolArr[c] === '+') {
        result = +result + +numArr[c+1];
    } else {
        result = +result - +numArr[c+1];
    } 
}
return result;
}



















  // bodmasCal(array, sign, cal) {
  //   let value = 0;
  //   sign.forEach(element => {
  //     let val1 = array[element - 1];
  //     let val2 = array[element];
  //     array.splice(element - 1, element);
  //     if (cal === 'd') {
  //       value = val1 / val2;
  //       array.splice(element, 0 , value);
  //     } else if (cal === 'm') {
  //       value = val1 * val2;
  //       array.splice(element, 0 , value);
  //     } else if (cal === 'a') {
  //       value = val1 + val2;
  //       array.splice(element, 0 , value);
  //     } else {
  //       value = val1 - val2;
  //       array.splice(element, 0 , value);
  //     }
  //   });
  // }
}
